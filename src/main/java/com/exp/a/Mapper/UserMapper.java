package com.exp.a.Mapper;

import com.exp.a.Pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper
{
    //根据产品编号查询
    @Select("select * from t_product where id= #{id} ")
    User get(int no);
    //根据状态查询
    @Select("select * from t_product where status= #{status} ")
    User get2(int status);
    //产品名称查询
    @Select("select * from t_product where name like concat('%',@name.'%')")
    User get3(String name);
    //新增商品
    @Select("=insert into  a values (#{id},{product_no},{product_name},{product_type},{status})")
    int insert(User user);
    //更新数据
    @Select("update a set product_no=#{product_no},product_name=#{product_name},product_type=#{product_type},status=#{status} where id=#{id}")
    int updateUser(User user);
}
