package com.exp.a.Controller;

import com.exp.a.Pojo.User;
import com.exp.a.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
@CrossOrigin
public class UserController
{
    @Autowired
    private UserService userService;

    //产品编号查询
    @RequestMapping("/FindNo/{No}")
    public String FindNo(Model model, @PathVariable int No)
    {
        User users = userService.get(No);
        model.addAttribute("users", users);
        return "产品编号查询成功，存在该商品";
    }

    //状态查询
    @RequestMapping("/FindStarus/{Status}")
    public String FindStarus(Model model, @PathVariable int Status)
    {
        User users = userService.get2(Status);
        model.addAttribute("users", users);
        return "状态查询成功";
    }

    //产品名称查询
    @RequestMapping("/FindName/{Name}")
    public String FindName(Model model, @PathVariable String Name)
    {
        User users = userService.get3(Name);
        model.addAttribute("users", users);
        return "产品名称查询成功，存在该商品";
    }

    //添加产品
    @RequestMapping("/insert/{id},{product_no},{product_name},{product_type},{status}")
    public String insert(User user)
    {
        userService.insert(user);
        return "新增数据成功";
    }

    //修改产品
    @PostMapping("/updateUser/{id},{product_no},{product_name},{product_type},{status}")
    public String updateUser(Model model, User user, HttpServletRequest request)
    {
        userService.updateUser(user);
        return "信息修改成功";

    }

}
