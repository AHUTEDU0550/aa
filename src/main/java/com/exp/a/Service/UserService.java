package com.exp.a.Service;

import com.exp.a.Pojo.User;

public interface UserService
{
    //根据产品编号查询
    User get(int no);
    //根据状态查询
    User get2(int status);
    //产品名称查询
    User get3(String name);

    //新增数据
    int insert(User user);
    //更新数据
    int updateUser(User user);
}
