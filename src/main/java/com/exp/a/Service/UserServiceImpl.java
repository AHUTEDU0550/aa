package com.exp.a.Service;

import com.exp.a.Mapper.UserMapper;
import com.exp.a.Pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService
{
    @Autowired
    private UserMapper userMapper;

    @Override
    //根据产品编号查询
    public User get(int no)
    {
        return userMapper.get(no);

    }
    //根据状态查询
    @Override
    public User get2(int status)
    {
        return userMapper.get2(status);
    }
    //产品名称查询
    @Override
    public User get3(String name)
    {
        return userMapper.get3(name);
    }
    //新增数据
    @Override
    public int insert(User user)
    {
       return userMapper.insert(user);
    }
    //更新数据
    @Override
    public int updateUser(User user)
    {
        return userMapper.updateUser(user);

    }


}
