package com.exp.a.Pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User
{
    private Integer id;//序号
    private Integer product_no;//产品编号
    private String product_name;//名称
    private String product_type;//产品类型
    private Integer Staus;//状态
}
